
import model.Person;
import model.Staff;
import model.Student;

public class App {
    public static void main(String[] args) throws Exception {
        Person person1 = new Person("Tam", "1234");
        Person person2 = new Person("Tam2", "4565");
        System.out.println(person1);
        System.out.println(person2);
        Student student1 = new Student("tam3", "13444", "English", 2, 200.00);
        Student student2 = new Student("tam4", "13444", "Math", 2, 200.00);
        System.out.println(student1);
        System.out.println(student2);

        Staff staff1 = new Staff("tam5", "3555", "Unit", 100.00);
        Staff staff2 = new Staff("tam6", "2566", "Unit2", 200.00);
        System.out.println(staff1);
        System.out.println(staff2);
    }
}
